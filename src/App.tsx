import React, { useState } from "react";
import "./App.css";

const listCiudades = ["Select All", "India", "USA", "France"];

const App: React.FC = () => {
  const [seleccionarCiudades, setseleccionarCiudades] = useState<string[]>([]);
  const cambiarCIudad = (Ciudad: string) => {
    if (Ciudad === "Select All") {
      setseleccionarCiudades((seleccionada) =>
        seleccionada.length === listCiudades.length - 1 ? [] : listCiudades.slice(1)
      );
    } else {
      const updatedlistCiudades = seleccionarCiudades.includes(Ciudad)
        ? seleccionarCiudades.filter((c) => c !== Ciudad)
        : [...seleccionarCiudades, Ciudad];
      setseleccionarCiudades(
        updatedlistCiudades.length === listCiudades.length - 1
          ? listCiudades.slice(1)
          : updatedlistCiudades
      );
    }
  };

  return (
    <div className="App">
      <h1>Checkbox de Países</h1>
      {listCiudades.map((Ciudad) => (
        <div key={Ciudad} className="checkbox">
          <input
            type="checkbox"
            id={Ciudad}
            checked={
              Ciudad === "Select All"
                ? seleccionarCiudades.length === listCiudades.length - 1
                : seleccionarCiudades.includes(Ciudad)
            }
            onChange={() => cambiarCIudad(Ciudad)}
          />
          <label htmlFor={Ciudad}>{Ciudad}</label>
        </div>
      ))}
      <div>
        <strong>Países Seleccionados:</strong> {seleccionarCiudades.join(", ")}
      </div>
    </div>
  );
};

export default App;
